import {
  tooltip,
  popover
} from './bootstrap-custom.js';
import {
  menuAPI
} from './after-load.js';

$(document).ready(function() {
  menuAPI();
});
$(function() {
  tooltip();
  popover();
});