function tooltip() {
  $("[data-toggle='tooltip']").tooltip();
}

function popover() {
  $("[data-toggle='popover']").popover();
}

export {
  tooltip,
  popover
}