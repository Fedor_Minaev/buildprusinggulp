  let menuAPI = () => {
    $('#menu1').click(() => showInfoPopup('/api/first'));
    $('#menu2').click(() => showInfoPopup('/api/second'));
    let showInfoPopup = (url) => {
      let id = 'info-popup' + parseInt(getRandomId(0, 100));
      let infoPopupObj = new Object;
      $.get(url, (msg) => {
        $('#info-popup').append("<div id='" + id + "' class='alert alert-info' style='display:none;'>" + msg + ".</div>");
        infoPopupObj = $('#' + id);
        setTimeout(() => infoPopupObj.fadeIn('slow'), 500);
        setTimeout(() =>
          infoPopupObj.fadeOut(1000, () => infoPopupObj.remove()), 5000);
      });


    }
    let getRandomId = (min, max) => Math.random() * (max - min) + min;
  }
  export {
    menuAPI
  }