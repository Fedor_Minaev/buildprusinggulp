var express = require('express');
var app = express()

app.get('/api/first', function (req, res) {
	res.send('API-first works properly')
})

app.get('/api/second', function (req, res) {
	res.send('API-second works properly')
})

app.listen(3000, function () {
	console.log('Example app listening on port 3000!')
})
