//add plugins(подключаем нужные плагины)	
var gulp = require('gulp'),
  minify = require('gulp-minify-css'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  inject = require('gulp-inject'),
  //del = require('del'),
  run_sequence = require('run-sequence'),
  //watch = require('gulp-watch');
  sass = require('gulp-sass'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  vinyl_buffer = require('vinyl-buffer'),
  vinyl_source = require('vinyl-source-stream'),
  //tsify = require('tsify'),
  //beautify_js = require('gulp-beautify');  --- не нужно
  beautify = require('gulp-jsbeautifier'),
  connect = require('gulp-connect'),
  proxy = require('http-proxy-middleware'),
  minimist = require('minimist'),
  gulpif = require('gulp-if'),
  exec = require('child_process').exec,
  gulpConfig = require('./gulp.config');
//create js object with work-path(создаем js объект с нужными для нашего проекта путями)

// в зависимости от среду присваеваем внутреннему объекту среды объект из конфиг файла.
var args = minimist(process.argv.slice(2));
var env;
getEnv: for (arg in args) {
  for (envConfig in gulpConfig.env) {
    if (arg.toString() == envConfig.toString()) {
      env = gulpConfig.env[envConfig];
      break getEnv;
    }
  }
}
if (!env) env = gulpConfig.env.development;

var src = gulpConfig['src'];
var watch = gulpConfig['watch'];

// прогоняем код через beautifire для удобства чтения
gulp.task('beautify-gulpfile', function(callback) {
  gulp.src(src.beautify.gulpfile.in)
    .pipe(beautify({
      indent_size: 2
    }))
    .pipe(gulp.dest(src.beautify.gulpfile.out));
  callback();
})

// прогоняем код через beautifire для удобства чтения
gulp.task('beautify-js', function(callback) {
  gulp.src(src.beautify.js.in)
    .pipe(beautify({
      indent_size: 2
    }))
    .pipe(gulp.dest(src.beautify.js.out));
  callback();
})

// прогоняем код через beautifire для удобства чтения
gulp.task('beautify-css', function(callback) {
  gulp.src(src.beautify.styles.in)
    .pipe(beautify({
      indent_size: 2
    }))
    .pipe(gulp.dest(src.beautify.styles.out));
  callback();
})

// прогоняем код через beautifire для удобства чтения
gulp.task('beautify-html', function(callback) {
  gulp.src(src.beautify.html.in)
    .pipe(beautify({
      indent_size: 2
    }))
    .pipe(gulp.dest(src.beautify.html.out));
  callback();
})

gulp.task('beautify', ['beautify-css', 'beautify-js', 'beautify-html', 'beautify-gulpfile']);

gulp.task('app-styles', /*['beautify-css'],*/ function(callback) { //перед выполнением прогоняем через beautifire  *** не получилось подружить с watch -> выделил в отдельную задачу
  return gulp.src(src.appStyles)
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    //в зависимоти от среды собираем стили по разному
    .pipe(gulpif(env.appStyles.minify, minify()))
    //.pipe(sourcemaps.write())
    .pipe(gulpif(env.appStyles.concat, concat(env.appStyles.concatName)))
    .pipe(gulp.dest(env.appStyles.dest));
});

gulp.task('vendor-styles', function(callback) {
  return gulp.src(src.vendorStyles)
    //В зависимости от среды собираем стили по разному
    .pipe(gulpif(env.vendorStyles.minify, minify()))
    .pipe(gulpif(env.vendorStyles.concat, concat(env.vendorStyles.concatName)))
    .pipe(gulp.dest(env.vendorStyles.dest));
});

gulp.task('app-scripts', /*['beautify-js'],*/ function(callback) { //перед выполнением прогоняем через beautifire  *** не получилось подружить с watch -> выделил в отдельную задачу
  return browserify(src.jsBrowserify)
    .transform(babelify, {
      presets: ['es2015']
    })
    .bundle()
    .pipe(vinyl_source(src.vinylSource))
    .pipe(vinyl_buffer())
    .pipe(gulpif(env.appJs.sourcemaps, sourcemaps.init({
      loadMaps: true
    })))
    .pipe(uglify())
    .pipe(gulpif(env.appJs.sourcemaps, sourcemaps.write(env.appJsSourcemapsDest)))
    .pipe(gulp.dest(env.appJs.dest));
});

gulp.task('vendor-scripts', function(callback) {
  return gulp.src(src.vendorJs)
    .pipe(gulpif(env.vendorJs.uglify, uglify()))
    .pipe(gulpif(env.vendorJs.concat, concat(env.vendorJs.concatName)))
    .pipe(gulp.dest(env.vendorJs.dest));
});

gulp.task('app-index', /*['beautify-html'],*/ function(callback) { //перед выполнением прогоняем через beautifire  *** не получилось подружить с watch -> выделил в отдельную задачу
  var injectSource = gulp.src(env.index.injectSrc, {
    read: false
  });
  var injectOption = {
    ignorePath: env.index.injectOptionIgnorePath,
    addRootSlash: false
  };
  return gulp.src(src.indexSrc)
    .pipe(inject(injectSource, injectOption))
    .pipe(gulp.dest(env.index.dest));
});

gulp.task('clean', function(callback) {
  exec('rm ' + env.delRootDirSrc + '/* -r');
  callback();
});

gulp.task('build', function(callback) {
  run_sequence(
    'clean', ['app-styles', 'app-scripts', 'vendor-styles', 'vendor-scripts'],
    'app-index',
    callback
  );
});

gulp.task('watch', function() {
  gulp.watch(watch.styles, ['app-styles', 'html']);
  gulp.watch(watch.js, ['app-scripts', 'html']);
  gulp.watch(watch.index, ['app-index', 'html']);
});

gulp.task('connect', function(callback) {
  return connect.server({
    root: env.serverRoot,
    livereload: true,
    port: 5000,
    middleware: function(connect, opt) {
      return [
        proxy('/api/', {
          target: 'http://localhost:3000/',
          changeOrigin: true,
          ws: true
        })
      ]
    }
  });
});

gulp.task('html', function(callback) {
  return gulp.src(env.htmlSrcLivereload)
    .pipe(connect.reload());
});

gulp.task('server', function(callback) {
  require('./server.js');
  callback();
});

gulp.task('default', /*['build', 'connect', 'server', 'watch'],*/ function(callback) {
  run_sequence(
    'build', ['connect',
      'server',
      'watch'
    ]
  );
});